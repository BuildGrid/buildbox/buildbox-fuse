cmake_minimum_required(VERSION 3.6)
project(buildbox-fuse C CXX)

find_package(PkgConfig REQUIRED)
pkg_check_modules(fuse REQUIRED IMPORTED_TARGET fuse3)
pkg_check_modules(uuid REQUIRED IMPORTED_TARGET uuid)

find_package(BuildboxCommon REQUIRED)

find_package(gRPC)
if(gRPC_FOUND)
    set(GRPC_TARGET gRPC::grpc++)
    set(GRPC_CPP_PLUGIN $<TARGET_FILE:gRPC::grpc_cpp_plugin>)
else()
    pkg_check_modules(gRPC REQUIRED IMPORTED_TARGET grpc++>=1.10)
    set(GRPC_TARGET PkgConfig::gRPC)
    find_program(GRPC_CPP_PLUGIN grpc_cpp_plugin)
endif()

if(NOT DEFINED PROTOBUF_TARGET)
    message(FATAL_ERROR "PROTOBUF_TARGET undefined, should be from BuildboxCommon.")
endif()

set(SRCS
    buildbox.cc
    client.cc
)

add_executable(buildbox-fuse ${SRCS})
install(TARGETS buildbox-fuse RUNTIME DESTINATION bin)
target_link_libraries(buildbox-fuse
    PkgConfig::fuse
    PkgConfig::uuid
    Buildbox::buildboxcommon
    ${PROTOBUF_TARGET}
    ${GRPC_TARGET})

include(CTest)

option(ALLOW_PARTIAL_TESTS "Run only tests that can be run given the host environment")

if(BUILD_TESTING)
    find_program(FUSE_LOCAL "tests/fuse-local.sh" PATHS ${CMAKE_CURRENT_SOURCE_DIR} REQUIRED NO_DEFAULT_PATH)
    find_program(FUSE_REMOTE "tests/fuse-remote.sh" PATHS ${CMAKE_CURRENT_SOURCE_DIR} REQUIRED NO_DEFAULT_PATH)
    find_program(EXTENDED_ATTRIBUTES "tests/fuse-xattr.sh" PATHS ${CMAKE_CURRENT_SOURCE_DIR} REQUIRED NO_DEFAULT_PATH)
    find_program(OUTPUT_TIMES "tests/output-times.sh" PATHS ${CMAKE_CURRENT_SOURCE_DIR} REQUIRED NO_DEFAULT_PATH)

    #
    # tools used in all tests
    #
    find_program(SHA256SUM sha256sum REQUIRED)
    find_program(FUSERMOUNT3 fusermount3 REQUIRED)

    if(ALLOW_PARTIAL_TESTS)
        find_program(SOCAT socat) # for systemd readiness check
        find_program(GETFATTR getfattr) # for xattr tests
        find_program(NETSTAT netstat) # for remote tests
        find_program(CAS_SERVER buildbox-casd) # for remote tests
    else()
        find_program(SOCAT socat REQUIRED)
        find_program(GETFATTR getfattr REQUIRED)
        find_program(NETSTAT netstat REQUIRED)
        find_program(CAS_SERVER buildbox-casd REQUIRED)
    endif()

    foreach(t
            unmodified
            create_empty_file
            create_file
            create_executable_file
            append_file
            truncate_file
            fallocate_file
            rename_file
            rename_file_crossdir
            rename_file_noreplace
            rename_file_noreplace_crossdir
            replace_file
            replace_file_crossdir
            remove_file
            create_empty_directory
            create_directory_with_file
            remove_directory
            create_symlink
            create_hardlink
            chown_ctime
            atime
            fifo
            chmod_perms)
        add_test(NAME fuse_local_${t} COMMAND ${FUSE_LOCAL} ${t})
        set_property(TEST fuse_local_${t} PROPERTY ENVIRONMENT BUILDBOX=$<TARGET_FILE:buildbox-fuse>)
        if(CAS_SERVER AND NETSTAT)
            add_test(NAME fuse_remote_${t} COMMAND ${FUSE_REMOTE} ${t})
            set_property(TEST fuse_remote_${t} PROPERTY ENVIRONMENT BUILDBOX=$<TARGET_FILE:buildbox-fuse> CAS_SERVER=${CAS_SERVER})
            add_test(NAME fuse_prefetch_${t} COMMAND ${FUSE_REMOTE} ${t})
            set_property(TEST fuse_prefetch_${t} PROPERTY ENVIRONMENT BUILDBOX=$<TARGET_FILE:buildbox-fuse> CAS_SERVER=${CAS_SERVER} PREFETCH=1)
        endif()
    endforeach()

    if(GETFATTR)
        add_test(NAME extended_attributes COMMAND ${EXTENDED_ATTRIBUTES})
        set_property(TEST extended_attributes PROPERTY ENVIRONMENT BUILDBOX=$<TARGET_FILE:buildbox-fuse> PROTO_PATH=${CMAKE_CURRENT_SOURCE_DIR})
    endif()

    add_test(NAME output_times COMMAND ${OUTPUT_TIMES})
    set_property(TEST output_times PROPERTY ENVIRONMENT BUILDBOX=$<TARGET_FILE:buildbox-fuse> PROTO_PATH=${CMAKE_CURRENT_SOURCE_DIR})

    if(CAS_SERVER AND NETSTAT)
        add_test(NAME time_output_remote COMMAND ${OUTPUT_TIMES})
        set_property(TEST time_output_remote PROPERTY ENVIRONMENT BUILDBOX=$<TARGET_FILE:buildbox-fuse> CAS_SERVER=${CAS_SERVER} PREFETCH=1 PROTO_PATH=${CMAKE_CURRENT_SOURCE_DIR})
    endif()
endif()

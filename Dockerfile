FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest

WORKDIR /app

RUN apt-get update && apt-get install -y \
    attr \
    cmake \
    fuse3 \
    gcc \
    g++ \
    git  \
    grpc++ \
    libfuse3-dev \
    libssl-dev \
    pkg-config \
    socat \
    uuid-dev \
    && apt-get clean

COPY . /buildbox/

WORKDIR /buildbox

RUN mkdir build && cd build && cmake -DBUILD_TESTING=OFF .. && make && cp buildbox-fuse /usr/bin/ && buildbox-fuse --help
